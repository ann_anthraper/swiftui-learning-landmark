//
//  Landmark.swift
//  SwiftUI Essentials
//
//  Created by Ann Mary on 04/01/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import SwiftUI
import CoreLocation

struct Coordinates: Hashable, Codable {
    var latitude: Double
    var longitude: Double
}

struct Landmark:Hashable, Codable, Identifiable {
    var id: Int
    var name: String
    var imageName: String
    var coordinates: Coordinates
    var state: String
    var park: String
    var category: Category
    
    var locationCoordinate: CLLocationCoordinate2D {
        CLLocationCoordinate2D(
            latitude: coordinates.latitude,
            longitude: coordinates.longitude)
    }
    
    enum Category: String, CaseIterable, Codable, Hashable {
        case featured = "Featured"
        case lakes = "Lakes"
        case rivers = "Rivers"
        case mountains = "Mountains"
    }
    
    #if DEBUG
    static let example = Landmark(id: 1001, name: "Turtle Rock", imageName: "turtlerock", coordinates: Coordinates(latitude: -116.16686, longitude: 34.011286), state: "California", park: "Joshua Tree National Park", category: Category(rawValue: "Featured")!)
    
    #endif
    
}


extension Landmark {
    var image: Image {
        ImageStore.shared.image(name: imageName)
    }
}


