//
//  LandmarkList.swift
//  SwiftUI Essentials
//
//  Created by Ann Mary on 04/01/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import SwiftUI

struct LandmarkList: View {
    
    let landmarks = Bundle.main.decode([Landmark].self, from: "landmarkData.json")
    
    var body: some View {
        NavigationView {
            List {
                ForEach(landmarks) { landmark in
                    LandmarkRow(landmark: landmark)
                }
            }
            .navigationBarTitle("Landmark")
        }
    }
}


struct LandmarkList_Previews: PreviewProvider {
    static var previews: some View {
        LandmarkList()
    }
}
