//
//  LandmarkRow.swift
//  SwiftUI Essentials
//
//  Created by Ann Mary on 04/01/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//
import SwiftUI

struct LandmarkRow: View {
    
    var landmark: Landmark
    
    var body: some View {
        NavigationLink(destination:LandmarkDetail(landmark: landmark)) {
            HStack {
                Image(landmark.imageName)
                    .resizable()
                    .frame(width: 50, height: 50)
                    .clipShape(Circle())
                    .overlay(Circle().stroke(Color.gray, lineWidth: 2))
                
                VStack(alignment: .leading) {
                    Text(landmark.name)
                        .font(.headline)
                }.layoutPriority(1)
                
                Spacer()
                
            }
        }
    }
}

struct LandmarkRow_Previews: PreviewProvider {
    static var previews: some View {
        LandmarkRow(landmark: Landmark.example)
    }
}
