//
//  LandmarkDetail.swift
//  SwiftUI Essentials
//
//  Created by Ann Mary on 04/01/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import SwiftUI

import SwiftUI

struct LandmarkDetail: View {
    
    var landmark: Landmark
    
    var body: some View {
        VStack {
            MapView(coordinate: landmark.locationCoordinate)
                .edgesIgnoringSafeArea(.top)
                .frame(height: 300)

            CircleImage(image: landmark.image)
                .offset(y: -130)
                .padding(.bottom, -130)

            VStack(alignment: .leading) {
                Text(landmark.name)
                    .font(.title)

                HStack(alignment: .top) {
                    Text(landmark.park)
                        .font(.subheadline)
                    Spacer()
                    Text(landmark.state)
                        .font(.subheadline)
                }
            }
            .padding()

            Spacer()
        }
    }
}
//        var body: some View {
//            VStack {
//                MapView(coordinate: landmark.locationCoordinate)
//                    .edgesIgnoringSafeArea(.top)
//                    .frame(height: 300)
//
//                CircleImage(image: landmark.image)
//                    .offset(x: 0, y: -130)
//                    .padding(.bottom, -130)
//
//                VStack(alignment: .leading) {
//                    Text(landmark.name)
//                        .font(.title)
//
//                    HStack(alignment: .top) {
//                        Text(landmark.park)
//                            .font(.subheadline)
//                        Spacer()
//                        Text(landmark.state)
//                            .font(.subheadline)
//                    }
//                }
//                .padding()
//
//                Spacer()
//            }
//            .navigationBarTitle(Text(verbatim: landmark.name), displayMode: .inline)
//        }



struct LandmarkDetail_Previews: PreviewProvider {
    static var previews: some View {
        LandmarkDetail(landmark: Landmark.example)
    }
}
